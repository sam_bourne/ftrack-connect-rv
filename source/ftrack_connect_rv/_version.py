# :coding: utf-8
# :copyright: Copyright (c) 2015 ftrack

# Note: No patch version is used as `.rvpkg` plugins can only contain versions
# in MAJOR.MINOR format.
__version__ = '3.6'
